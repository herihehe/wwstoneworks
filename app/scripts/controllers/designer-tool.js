'use strict';

/**
 * @ngdoc function
 * @name wwstoneworksApp.controller:DesignerTool
 * @description
 * # DesignerTool
 * Controller of the wwstoneworksApp
 */
angular.module('wwstoneworksApp')
  .controller('DesignerTool', ['$rootScope', '$state', function ($rootScope, $state) {
    $rootScope.template = {
    	fullName : 'Charlotte Martinez',
    	dateOfBirth: '1979-02-03',
    	dateOfDeceased: '2009-03-21',
    	epitaph: 'I will be in the wind that moves by you',
    	material: 1
    };

    $rootScope.$watch( function(){
    	$rootScope.currentPage = $state.$current.name;
    }, function(){}, true);
  }]);
