'use strict';

/**
 * @ngdoc function
 * @name wwstoneworksApp.controller:DesignerToolMaterial
 * @description
 * # DesignerToolMaterial
 * Controller of the wwstoneworksApp
 */
angular.module('wwstoneworksApp')
  .controller('DesignerToolMaterial', ['$scope', function ($scope) {
    $scope.materials = [
    	{
    		id: 1,
    		name: 'American Black',
    		thumbnail: 'AmericanBlack.png'
    	},
    	{
    		id: 2,
    		name: 'Antique Rose',
    		thumbnail: 'AntiqueRose.png'
    	},
    	{
    		id: 3,
    		name: 'Apache Red',
    		thumbnail: 'ApacheRed.png'
    	},
    	{
    		id: 4,
    		name: 'Autumn Mist',
    		thumbnail: 'AutumnMist.png'
    	},
    	{
    		id: 5,
    		name: 'Autumn Rose',
    		thumbnail: 'AutumnRose.png'
    	},
    	{
    		id: 6,
    		name: 'Bahama Blue',
    		thumbnail: 'BahamaBlue.png'
    	},
    	{
    		id: 7,
    		name: 'Balmoral Fine Grain',
    		thumbnail: 'BalmoralFineGrain.png'
    	},
    	{
    		id: 8,
    		name: 'Bellingham',
    		thumbnail: 'Bellingham.png'
    	},
    	{
    		id: 9,
    		name: 'Black Standard',
    		thumbnail: 'BlackStandard.png'
    	},
    	{
    		id: 10,
    		name: 'Black Ultimate',
    		thumbnail: 'BlackUltimate.png'
    	},
    	{
    		id: 11,
    		name: 'Blue Pearl',
    		thumbnail: 'BluePearl.png'
    	},
    	{
    		id: 12,
    		name: 'Britz Impala Black',
    		thumbnail: 'BritzImpalaBlack.png'
    	},
    	{
    		id: 13,
    		name: 'Canadian Green',
    		thumbnail: 'CanadianGreen.png'
    	},
    	{
    		id: 14,
    		name: 'Canadian Mahogany',
    		thumbnail: 'CanadianMahogany.png'
    	},
    	{
    		id: 15,
    		name: 'Canadian Pink',
    		thumbnail: 'CanadianPink.png'
    	},
    	{
    		id: 16,
    		name: 'Cats Eye Red',
    		thumbnail: 'CatsEyeRed.png'
    	},
    	{
    		id: 17,
    		name: 'China Green',
    		thumbnail: 'ChinaGreen.png'
    	},
    	{
    		id: 18,
    		name: 'China Mahogany',
    		thumbnail: 'ChinaMahogany.png'
    	},
    	{
    		id: 19,
    		name: 'China Pink',
    		thumbnail: 'ChinaPink.png'
    	},
    	{
    		id: 20,
    		name: 'Mill Lane',
    		thumbnail: 'MillLane.png'
    	}
    ];
  }]);
