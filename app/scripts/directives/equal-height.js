'use strict';

/**
 * @ngdoc function
 * @name wwstoneworksApp.directive:equalHeight
 * @description
 * # equalHeight
 */
angular.module('wwstoneworksApp')
  .directive('equalHeight', function ($timeout) {
    return{
    	restrict: 'A',
    	scope: {
    		equalTo: '@'
    	},
    	link: function(scope, element){
    		$timeout(function(){
    			scope.baseHeight = angular.element(scope.equalTo)[0].clientHeight;
    			element.css('height', scope.baseHeight);
    		}, 100);
    	}
    };
  });
