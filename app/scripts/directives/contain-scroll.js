'use strict';

/**
 * @ngdoc function
 * @name wwstoneworksApp.directive:containScroll
 * @description
 * # containScroll
 */
angular.module('wwstoneworksApp')
  .directive('containScroll', function ($window) {
    return{
    	restrict: 'C',
    	link: function(scope, element){
    		$window.Ps.initialize(element[0]);
    	}
    };
  });
