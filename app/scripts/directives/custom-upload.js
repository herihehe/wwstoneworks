'use strict';

/**
 * @ngdoc function
 * @name wwstoneworksApp.directive:customUpload
 * @description
 * # dropzone
 */
angular.module('wwstoneworksApp')
  .directive('customUpload', function () {
    return{
    	restrict: 'A',
    	scope: {
    		url: '@',
            thumbnailWidth: '@',
            thumbnailHeight: '@',
            maxFiles: '@',
            acceptedFiles: '@',
            clickable: '@',
            previewTemplate: '@'
    	},
    	link: function(scope, element){
    		if ( element.is('.dropzone-init') ){
                return false;
            }
            var el = {
                _field              : element,
                _action             : scope.url,
                _thumbnailWidth     : scope.thumbnailWidth,
                _thumbnailHeight    : scope.thumbnailHeight,
                _maxFiles           : scope.maxFiles,
                _acceptedFiles      : scope.acceptedFiles
            };
            element.dropzone({
                url                 : el._action,
                thumbnailWidth      : el._thumbnailWidth || null,
                thumbnailHeight     : el._thumbnailHeight || null,
                maxFiles            : el._maxFiles || 1,
                acceptedFiles       : el._acceptedFiles,
                clickable           : scope.clickable,
                previewTemplate     : el._field.find(scope.previewTemplate).html(),
                init                : function(){
                    this
                        .on('processing', function(){
                            el._field.addClass('dz-processing');
                            el._field.removeClass('dz-complete dz-dragover');
                        })
                        .on('complete', function(file){
                            el._field.addClass('dz-complete');
                            el._field.removeClass('dz-processing dz-dragover');
                        })
                        .on('removedfile', function() {
                            el._field.removeClass('dz-processing dz-complete dz-dragover');
                        })
                        .on('dragover', function() {
                            el._field.addClass('dz-dragover');
                        })
                        .on('thumbnail', function(file, src) {
                            var img = angular.element(new Image()).attr('src', src);
                            img.imageMask('/images/mask-portrait.png');
                            element.find('.contain-upload-thumbnail').find('img').remove();
                            element.find('.contain-upload-thumbnail').prepend(img);
                        })
                        .on('totaluploadprogress', function(progress) {
                            var text = Math.round(progress) === 100 ? 'Please wait &#8230;' : 'Uploading ' + Math.round(progress) + '%';
                            el._field.find('.progress-number').html(text);
                            el._field.find('.progress-radial').removeAttr('class').addClass('progress-radial progress-' + Math.ceil(progress/5)*5);
                        });
                }
            });
            el._field.addClass('dropzone-init');
    	}
    };
  });
