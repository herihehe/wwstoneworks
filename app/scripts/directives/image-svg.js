'use strict';

/**
 * @ngdoc function
 * @name wwstoneworksApp.directive:imageSvg
 * @description
 * # imageSvg
 * SVGInjector library wrapper
 */
angular.module('wwstoneworksApp')
  .directive('imageSvg', function ($window) {
    return{
    	restrict: 'C',
    	link: function(scope, element){
    		$window.SVGInjector(element);
    	}
    };
  });
