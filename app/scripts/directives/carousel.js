'use strict';

/**
 * @ngdoc function
 * @name wwstoneworksApp.directive:carousel
 * @description
 * # owl.carousel
 */
angular.module('wwstoneworksApp')
  .directive('carousel', function () {
    return{
    	restrict: 'A',
    	scope: {
    		options: '@'
    	},
    	link: function(scope, element){
    		scope.options = JSON.parse(scope.options);
            scope.carousel = element.owlCarousel(scope.options);
            element.find('.carousel-start').click(function(){
                scope.carousel.trigger('to.owl.carousel', 1);
            });
            element.find('.carousel-to').click(function(evt){
                scope.carousel.trigger('to.owl.carousel', 
                    evt.currentTarget.dataset.carouselTo);
            });
    	}
    };
  });
