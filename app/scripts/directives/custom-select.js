'use strict';

/**
 * @ngdoc function
 * @name wwstoneworksApp.directive:customSelect
 * @description
 * # chosen select
 */
angular.module('wwstoneworksApp')
  .directive('customSelect', function () {
    return{
    	restrict: 'A',
        scope: {
            'noSearch': '@'
        },
    	link: function(scope, element){
    		element.chosen({
                disable_search_threshold: 9999
            });
    	}
    };
  });
