'use strict';

/**
 * @ngdoc function
 * @name wwstoneworksApp.directive:tooltip
 * @description
 * # bootstrap.tooltip
 */
angular.module('wwstoneworksApp')
  .directive('tooltip', function () {
    return{
    	restrict: 'A',
    	link: function(scope, element){
    		element.tooltip();
    	}
    };
  });
