'use strict';

/**
 * @ngdoc overview
 * @name wwstoneworksApp
 * @description
 * # wwstoneworksApp
 *
 * Main module of the application.
 */
angular
  .module('wwstoneworksApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider
      .otherwise('/');

    $stateProvider
      .state('home', {
        url: '/',
        controller: 'Home',
        templateUrl: 'views/home.html'
      })

      .state('monuments', {
        url: '/',
        controller: 'Home',
        templateUrl: 'views/monuments.html'
      })

      .state('signIn', {
        url: '/sign-in',
        controller: 'Home',
        templateUrl: 'views/sign-in.html'
      })

      .state('signUp', {
        url: '/sign-up',
        controller: 'Home',
        templateUrl: 'views/sign-up.html'
      })

      .state('checkout', {
        url: '/checkout',
        controller: 'Home',
        templateUrl: 'views/checkout.html'
      })

      .state('account', {
        url: '/account',
        controller: 'Home',
        templateUrl: 'views/account.html'
      })

      .state('designerTool', {
        abstract: true,
        url: '/designer-tool',
        controller: 'DesignerTool',
        templateUrl: 'views/designer-tool/index.html'
      })

        .state('designerTool.inscription', {
          url: '/inscription',
          controller: 'DesignerToolInscription',
          templateUrl: 'views/designer-tool/inscription.html'
        })

        .state('designerTool.material', {
          url: '/material',
          controller: 'DesignerToolMaterial',
          templateUrl: 'views/designer-tool/material.html'
        })

        .state('designerTool.artwork', {
          url: '/artwork',
          controller: 'Home',
          templateUrl: 'views/designer-tool/art/index.html'
        })

        .state('designerTool.collection', {
          url: '/artwork-collection',
          controller: 'Home',
          templateUrl: 'views/designer-tool/art/collection.html'
        })

        .state('designerTool.uploadPortrait', {
          url: '/upload-portrait',
          controller: 'Home',
          templateUrl: 'views/designer-tool/upload-portrait.html'
        })

        .state('designerTool.accessories', {
          url: '/accessories',
          controller: 'Home',
          templateUrl: 'views/designer-tool/accessories-base.html'
        })

        .state('designerTool.accessoriesVase', {
          url: '/accessories-vase',
          controller: 'Home',
          templateUrl: 'views/designer-tool/accessories-vase.html'
        })

        .state('designerTool.finalPreview', {
          url: '/final-preview',
          controller: 'Home',
          templateUrl: 'views/designer-tool/final-preview.html'
        });
  });
